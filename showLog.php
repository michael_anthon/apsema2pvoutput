<?php
ini_set('display_errors', 1);
require('config.php');

$lines = file_get_contents($logFile);

$lines = explode("\r\n", $lines);
$headers = explode("\t", $lines[0]);

$validDate = false;
if(isset($_REQUEST['date']))
{
  $dateFilter = date_parse_from_format( 'Ymd', $_REQUEST['date']);
  if($dateFilter['error_count'] == 0)
  {
    $dateFilter = sprintf('%d-%02d-%02d', $dateFilter['year'], $dateFilter['month'], $dateFilter['day']);
    $validDate = true;
  }

}


if (!$validDate)
{
  $dates = array();
  // No date given, build a list of dates and link to them
  foreach($lines as $k=>$v)
  {
    $dates[substr($v, 0, 10)] = 1;
  }
  krsort($dates);
  foreach($dates as $k=>$v)
  {
    $dateFilter = date_parse_from_format( 'Y-m-d', $k);
    if($dateFilter['error_count'] == 0)
      printf('<a href="%s?date=%s">%s</a><br>', $_SERVER['SCRIPT_NAME'], str_replace('-', '', $k), $k);

  }
  die();
}


// Extract the requested log data
//$logData = array_slice($lines, -12*24);  // 12 readings an hour, 12 hours
$logData = array();

foreach($lines as $k=>$v)
{
  if($dateFilter == substr($v, 0, 10))
  {
    $logData[$k] = explode("\t", $v);
  }

}

// Extract timestamps for the x axis labels

$datasets = array();
foreach(array_slice($headers,3) as $k=>$data)
{
    $datasets[$k]['label'] = $data;
}


foreach(array_slice($logData,1) as $kl=>$data)
{
  if(count($data) > 1)
  {
    $xLabels[$kl] = $data[0];

  }
  foreach(array_slice($data,3) as $kd=>$data)
  {
    $datasets[$kd]['data'][$kl] = $data;
  }

}

if (!isset($datasets[0]['data']))
{
  die("No data for $dateFilter");
}

$i = 0;
foreach($datasets as $dataset)
{
  $dataValues = implode(',',$dataset['data']);
  $colour = $colours[$i%count($colours)];
  $v[] = <<<DATA
  {
    label: "${dataset['label']}",
              fillColor: "rgba(0,0,0,0)",
              strokeColor: "${colour}",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: [${dataValues}]
          }
DATA;
  $i++;
}
$dataSetString = implode(',', $v);

$xLabels = '"'.implode('","', array_slice($xLabels,3)).'"';
//print "<pre>";
//var_dump($datasets);
//var_dump($v);
//
////var_dump($xLabels);
//die();
print <<<HTML
<html>
<head></head>
<body>
<script src="Chart.js"></script>

<canvas id="myChart" width="1600" height="800"></canvas>

<script type="text/javascript">
  var ctx = document.getElementById("myChart").getContext("2d");
  var data = {
      labels: [${xLabels}],
      datasets: [
          ${dataSetString}
      ]

  };
  var myLineChart = new Chart(ctx).Line(data, {pointDot : false});

</script>


HTML;

/*
print '<table border="1"><tr>';

foreach($headers as $header)
{
  print '<th>'.$header.'</th>';
}
print '</tr>';

for($i=1; $i<count($logData)-1; $i++)
{

  print '<tr>';
  foreach($logData[$i] as $value)
  {
    print '<td>'.$value.'</td>';
  }
  print "</tr>\r\n";
}

print '</table>';
*/
print "</body></html>";



