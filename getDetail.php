<?php
ini_set('display_errors', 1);

require('config.php');

function parseTable($html, $hasHeaders = true)
{
  // Find the table
  preg_match('/<table.*?>.*?<\/[\s]*table>/s', $html, $table_html);

  // Clean up the dodgy HTML from the EMU
  $table_html = $table_html[0];
  $table_html = str_replace('<center>', ' ', $table_html);
  $table_html = str_replace('</center>', ' ', $table_html);
  $table_html = str_replace('&nbsp;', ' ', $table_html);
  $table_html = str_replace('<sup>o</sup>', ' ', $table_html);
  $output = preg_replace('/<([^ >]+) [^>]*>/', '<\1>', $table_html);

  // Iterate each row
  preg_match_all('/<tr.*?>(.*?)<\/[\s]*tr>/s', $table_html, $rows);

  // Get title for each row
  preg_match_all('/<th.*?>(.*?)<\/[\s]*th>/', $table_html, $row_headers);
  $row_headers = $row_headers[1];
  // If there were none, assume the first row has the headers
  // (table wasn't formatted with TH tags)

  if(!(is_array($row_headers) && count($row_headers) == 1) && $hasHeaders)
  {
    preg_match_all('/<td.*?>(.*?)<\/[\s]*td>/', $rows[1][0], $row_headers);
    $row_headers = $row_headers[1];
    unset($rows[1][0]);
  }

  $table = array();

  foreach($rows[1] as $row_html)
  {
    preg_match_all('/<td.*?>(.*?)<\/[\s]*td>/', $row_html, $td_matches);
    $row = array();
    for($i=0; $i<count($td_matches[1]); $i++)
    {
      $td = strip_tags(html_entity_decode($td_matches[1][$i]));
      $row[$row_headers[$i]] = $td;
    }

    if(count($row) > 0)
      $table[] = $row;
  }
  return $table;
}

function getDetails($detailUrl)
{
  $ch = curl_init();

  curl_setopt($ch,CURLOPT_URL, $detailUrl);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

  $output = curl_exec($ch);
  curl_close($ch);

  return parseTable($output);

}

function getSummary($summaryUrl)
{

  $ch = curl_init();

  curl_setopt($ch,CURLOPT_URL, $summaryUrl);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

  $output = curl_exec($ch);
  curl_close($ch);

  preg_match('/<table.*<\/table>/', $output,  $bits);

  $table = $bits[0];

  $table = preg_replace('/<([^ ]+) [^>]*>/', '<\1>', $table);
  $table = str_replace('&nbsp;', ' ', $table);
  $table = str_replace('<sup>o</sup>', ' ', $table);

  $xml = new SimpleXMLElement($table);
  $table = json_decode(json_encode($xml), true);

  $summary['W'] = floatval($table['center'][2]['tr']['td'][1]);
  $summary['Wh']  = floatval($table['center'][3]['tr']['td'][1])*1000;

  return $summary;
}

function getAverage($details, $key, $min=0)
{
  $sum = 0;
  $count = 0;
  foreach($details as $detail)
  {
    $value = floatval(($detail[$key]));
    // If it's a sensible value then we can use it
    if($value >= $min)
    {
      $sum += $value;
      $count++;
    }
  }
  return ($count == 0)?0:($sum / $count);
}

$details = getDetails($detailUrl);
$summary = getSummary($summaryUrl);

$headers = array_flip(array_keys($details[0]));

$status['d'] = date("Ymd");
$status['t'] = date("H:i");
$status['v1'] = $summary['Wh'];
$status['v2'] = $summary['W'];
$status['v5'] = (int)getAverage($details, 'Grid Voltage', 50);
$status['v6'] = (int)getAverage($details, 'Temperature', 0);





var_dump($status);
/*
$status['v1'] = $summary['Wh'];
$status['v2'] = $summary['W'];

*/

